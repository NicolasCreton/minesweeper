/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur;


import controlor.ConsolePlay;
import controlor.InitializeGameConsole;
import model.PlateauDemineurModel;
import view.console.PlateauDeJeuConsoleView;
import view.graphique.NewGameFrame;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

public class Demineur {

    /**
     * Permet de lancer le jeu en graphique ou en console selon les arguments données
     * Si aucun -> graphique
     * Si console ou -c -> console
     * Change aussi le LAF en verifiant si Nimbus est bien installé sur la distribution
     * @param args the command line arguments
     *
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            try {
                for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                    if (!"Nimbus".equals(info.getName())) {
                        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    } else if ("Nimbus".equals(info.getName())) {
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "The look and Feel 'Nimbus' was not found on your system, set to default", "Warning", JOptionPane.WARNING_MESSAGE);
            }
            NewGameFrame ngf = new NewGameFrame();
        } else if (args[0].equals("--console") || args[0].equals("-c")) {
            System.out.println("Dévoiler la case i j : d i j");
            System.out.println("Marquer la case i j comme contenant une mine : m i j x ");
            System.out.println("Marquer la case i j comme étant indécise : m i j ?");
            System.out.println("Supprimer tout marquage sur la case i j : m i j # ");
            System.out.println("Quitter : 'q' puis ENTER");
            PlateauDemineurModel monPlateau = InitializeGameConsole.intitializeGame();
            PlateauDeJeuConsoleView consolePanel = new PlateauDeJeuConsoleView(monPlateau);
            consolePanel.affichagePlateauDeJeuConsole();
            ConsolePlay.play(monPlateau);
        } else if(args[0].equals("-?") || args[0].equals("-h") || args[0].equals("--help")){
            String msg = "Syntaxe : java -jar Demineur.jar \n" + 
                         "            (pour l'exécution graphique) \n" +
                         "         ou java -jar Demineur.jar [-option] \n" + 
                         "OPTIONS : \n" +
                         "       -c, --console      Lance le demineur en mode console\n" + 
                         "       -?, -h, -help      impression du message d'aide \n";
            System.out.println(msg);
            
        } else {
            String msg = "Demineur : " +
                         args[0] +
                    ": option non valable \n" +
                         "Syntaxe : java -jar Demineur.jar \n" + 
                         "            (pour l'exécution graphique) \n" +
                         "         ou java -jar Demineur.jar [-option] \n" + 
                         "OPTIONS : \n" +
                         "       -c, --console      Lance le demineur en mode console\n" + 
                         "       -?, -h, -help      impression du message d'aide \n";
            System.out.println(msg);
        }


    }






}
     
