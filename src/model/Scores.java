package model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static Tools.OsFinder.getOS;

/**
 * Created by Nicolas on 02/12/2014.
 */
public class Scores {

    private Date date;
    private ScoreItem sci;
    private File directory;
    private File fichier;

    /**
     * Permet de serializer les scores, c'est à dire les enregistrer dans un fichier nommé par sa date
     * Verifie si le programme tourne sous windows ou linux et adapte les chemins en fonction du stystème
     * Crée des dossiers en fonctions de la difficulté
     *
     * @param scoreItem
     */
    public Scores(ScoreItem scoreItem) {
        this.date = new Date();
        SimpleDateFormat sdt = new SimpleDateFormat("E yyyy.MM.dd hh_mm_ss a zzz");
        //date.toString();
        if (getOS().equals("win")) {
            this.directory = new File("src\\scores\\" + scoreItem.getDifficulty());
        } else {
            this.directory = new File("src/scores/" + scoreItem.getDifficulty());
        }

        if (!directory.exists()) {
            directory.mkdir();
        }

        if (getOS().equals("win")) {
            this.fichier = new File("src\\scores\\" + scoreItem.getDifficulty() + "\\" + sdt.format(date) + ".txt");
        } else {
            this.fichier = new File("src/scores/" + scoreItem.getDifficulty() + "/" + sdt.format(date) + ".txt");
        }
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fichier));
            oos.writeObject(scoreItem);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
