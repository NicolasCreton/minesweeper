package model;

//import java.util.Timer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

/**
 * Created by nicolas on 30/11/14.
 */
public class Chronometre extends Observable {
    private Timer chrono;
    private int secondes = 0;
    private int minutes = 0;
    private int totalTime = 0;


    /**
     * Classe chronomètre, permettant de compter le temps grace à l'utilisation d'un timer Swing
     * Elle est observable et notifie ses observers lors d'un incrémentation de temps.
     */
    public Chronometre() {
        this.chrono = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                secondes++;
                totalTime++;
                if (secondes == 60) {
                    secondes = 0;
                    minutes++;
                }
                setChanged();
                notifyObservers();
            }
        });
        this.chrono.start();
    }

    public void start() {
        this.chrono.start();
    }

    public String getSecondesString() {
        return String.valueOf(secondes);
    }

    public String getMinutesString() {
        return String.valueOf(minutes);
    }

    public int getSecondes() {
        return this.secondes;
    }

    public int getMinutes() {
        return minutes;
    }

    public void stop() {
        this.chrono.stop();
    }

    public Timer getChrono() {
        return chrono;
    }

    public int getTotalTime() {
        return totalTime;
    }
}
