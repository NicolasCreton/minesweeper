/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author worfoss
 */
public abstract class PlateauDeJeuModel extends Observable{
    private int nombreLigne;
    private int nombreColonne;
    private List<CaseModel> plateau;


    /**
     * Classe Plateu de jeu générique qui serivira à créer un plateau de cases pour notre demineur
     * Ce plateau est modélisé sous forme de Arraylist, qui est plus générique si nous voulons changer de type de démineur ou même de jeu.
     *
     * @param nombreLigne
     * @param nombreColonne
     */
    public PlateauDeJeuModel(int nombreLigne, int nombreColonne) {
        this.nombreLigne = nombreLigne;
        this.nombreColonne = nombreColonne;
        int taille = this.nombreColonne*this.nombreLigne;
        this.plateau = new ArrayList<CaseModel>();
        //remplissage des cases du tableau avec du "RIEN"
        for(int index = 0; index < taille; ++index){
            this.addCase(new CaseModel(true, false, EtatAffichageModel.RIEN));
        }
        
    }

    public int getNombreLigne() {
        return nombreLigne;
    }
    
    public void setNombreLigne(int nombreLigne) {
        this.nombreLigne = nombreLigne;
    }

    public int getNombreColonne() {
        return nombreColonne;
    }

    public void setNombreColonne(int nombreColonne) {
        this.nombreColonne = nombreColonne;
    }

    public List<CaseModel> getPlateau() {
        return plateau;
    }

    public void setPlateau(List<CaseModel> plateau) {
        this.plateau = plateau;
    }
    
    public int getSize(){
        return this.plateau.size(); 
   }
    
    public void addCase(CaseModel maCase){
        this.plateau.add(maCase);
    }
    
    public CaseModel getCase(int indexLigne, int indexColonne) {
        return this.plateau.get(indexLigne * this.getNombreColonne() + indexColonne);
    }
    
    public CaseModel getCasebyIndex(int index){
        return this.plateau.get(index);
    }
    
    public void setCase(int indexLigne, int indexColonne, CaseModel maCase) {
        this.plateau.set(indexLigne * this.getNombreColonne() + indexColonne, maCase);
        this.notify();
    }
    
}
