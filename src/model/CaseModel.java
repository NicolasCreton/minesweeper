/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Observable;

/**
 * Modèle représentant une case
 * La case connait son état, le nombre de mine autour et si elle est cachée
 * Cette clase est observable, elle notifie les observers lors du changement d'etat interne ou d'affichage (drapeau, indecis ou rien)
 */
public class CaseModel extends Observable{
    private boolean cacher;
    private boolean etatInterne;
    private EtatAffichageModel etatAffichage;
    private int nombreMineAutour;

    public CaseModel(boolean cacher, boolean etatInterne, EtatAffichageModel etatAffichage) {
        this.cacher = true;
        this.etatInterne = etatInterne;
        this.etatAffichage = etatAffichage;
        this.nombreMineAutour = 0;
    }

    public boolean isCacher() {
        return cacher;
    }

    public void setCacher(boolean cacher) {
        this.cacher = cacher;
        setChanged();
        notifyObservers();
    }

    public boolean isMine() {
        return etatInterne;
    }

    public void setEtatInterne(boolean etatInterne) {
        this.etatInterne = etatInterne;
        setChanged();
        notifyObservers();
    }

    public EtatAffichageModel getEtatAffichage() {
        return etatAffichage;
    }

    public void setEtatAffichage(EtatAffichageModel etatAffichage) {
        this.etatAffichage = etatAffichage;
        setChanged();
        notifyObservers();
    }

    public Integer getNombreMineAutour() {
        return nombreMineAutour;
    }

    public void setNombreMineAutour(int nombreMineAutour) {
        this.nombreMineAutour = nombreMineAutour;
    }   
    
}
