/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *Etat d'affichage d'une case
 *
 */
public enum EtatAffichageModel {
    RIEN,
    DRAPEAU,
    INDECIS
}
