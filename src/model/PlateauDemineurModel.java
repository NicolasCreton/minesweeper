/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class PlateauDemineurModel extends PlateauDeJeuModel{
    private int nombreMines;
    private int nombreCaseDevoiler;
    private int flagNumber;
    private Difficulty difficulty;
    private int wellMarkedCase = 0;
    private boolean isWon;
    private Chronometre chrono;
    private int totalTime;
    private String playerName;

    /**
     * Classe plateau de jeu pour notre démineur, héritant d'un plateau de jeu classique.
     *
     * @param nombreLigne
     * @param nombreColonne
     * @param nombreMines
     * @param difficulty
     */

    public PlateauDemineurModel(int nombreLigne, int nombreColonne, int nombreMines, Difficulty difficulty) {
        super(nombreLigne, nombreColonne);
        this.nombreMines = nombreMines;
        this.nombreCaseDevoiler = 0;
        this.flagNumber = 0;
        this.difficulty = difficulty;
        this.playerName = "Anonymous";
        chrono = new Chronometre();
        chrono.start();
    }

    /**
     * Permet de poser les mines aléatoirement sur le plateau de jeu.
     */
    public void poseDesMines(){
        int nombreMines = this.getNombreMine();
        int taille = this.getSize();
        //On commence par poser les mines
        while (nombreMines != 0) {
            int randMine = (int)( Math.random()*( taille - 0 )) + 0;
            if (this.getCasebyIndex(randMine).isMine()== false) {
                this.getCasebyIndex(randMine).setEtatInterne(true);
                nombreMines--;
            }
        }
        //Ensuite on calcule le nombre de mines autour de chaque cases
        for (int indexLigne = 0; indexLigne < this.getNombreLigne(); ++indexLigne) {
            for (int indexColonne = 0; indexColonne < this.getNombreColonne(); ++indexColonne) {
                this.calculNombreMineAutour(indexLigne, indexColonne);    
            }
            
        }
    }

    public int getNombreCaseDevoiler() {
        return nombreCaseDevoiler;
    }

    public void setNombreCaseDevoiler(int nombreCaseDevoiler) {
        
        this.nombreCaseDevoiler = nombreCaseDevoiler;
    }

    /**
     * Retourne le nombre de drapeau déjà posés sur le plateau
     * @return
     */
    public int getFlagNumber() {
        return flagNumber;
    }

    public void setFlagNumber(int nombreSupMine) {
        this.flagNumber = nombreSupMine;
        setChanged();
        notifyObservers();
    }

    public void decrementFlag() {
        this.flagNumber--;
        setChanged();
        notifyObservers();
    }
    
    public void incrementFlag() {
        this.flagNumber++;
        setChanged();
        notifyObservers();
    }

    /**
     * devoile récursimvement les cases autour d'une mine. S'arrête lorsque le nombre de mines est !=0
     * @param indexLigne
     * @param indexColonne
     */
    public void devoilerCaseAutourRecur(int indexLigne, int indexColonne){
        if (indexLigne >= 0 && indexColonne >= 0 && indexLigne < this.getNombreLigne() && indexColonne < this.getNombreColonne() ){
            if (this.getCase(indexLigne,indexColonne).isCacher()== true) {
                this.getCase(indexLigne, indexColonne).setCacher(false);
                if(this.getCase(indexLigne, indexColonne).getEtatAffichage() == EtatAffichageModel.DRAPEAU){
                    this.decrementFlag();
                }
                this.setNombreCaseDevoiler(this.getNombreCaseDevoiler() + 1);                

                if (this.getCase(indexLigne, indexColonne).getNombreMineAutour() == 0) {
                    this.devoilerCaseAutour(indexLigne-1, indexColonne);
                    this.devoilerCaseAutour(indexLigne+1, indexColonne);
                    this.devoilerCaseAutour(indexLigne, indexColonne+1);
                    this.devoilerCaseAutour(indexLigne, indexColonne-1);
                    this.devoilerCaseAutour(indexLigne+1, indexColonne+1);
                    this.devoilerCaseAutour(indexLigne+1, indexColonne-1);
                    this.devoilerCaseAutour(indexLigne-1, indexColonne+1);
                    this.devoilerCaseAutour(indexLigne-1, indexColonne-1);                    
                }
            }
        }
    }

    /**
     *Appelle la fonction recursive de dévoilement et notifie les observers d'un changement
     * @param indexLigne
     * @param indexColonne
     */
    public void devoilerCaseAutour(int indexLigne, int indexColonne){
        devoilerCaseAutourRecur(indexLigne, indexColonne);
        setChanged();
        notifyObservers();
    }
    


    public void setNombreMines(int nombreMines) {
        this.nombreMines = nombreMines;
    }

    /**
     * Verifie si le jeu est perdu ou non
     * @param ligne
     * @param colonne
     * @return
     */
    public boolean isLose(int ligne, int colonne){
        if(this.getCase(ligne, colonne).isMine() && this.getCase(ligne, colonne).isCacher() == false){
            return true;
        }
        return false;
    }

    /**
     * Permet de savoir si la partie est terminée
     *
     * @return
     */
    public boolean isNotTheEnd(){
        if ((this.getSize() - this.getNombreCaseDevoiler()) == this.getNombreMine() && this.getFlagNumber() == this.getNombreMine()) {
            return true;
        }
        return false;
    }


    /**
     * Permet de calculer le nombres de mines autour d'une case donnée
     * @param ligne
     * @param colonne
     */
    public void calculNombreMineAutour(int ligne, int colonne){
        int mine = 0;        
        for (int indexLigne = ligne-1; indexLigne <= ligne+1; ++indexLigne) {
            for(int indexColonne = colonne-1; indexColonne <= colonne+1; ++indexColonne) {
                    if (indexLigne >= 0 && indexColonne >= 0 && indexLigne < this.getNombreLigne() && indexColonne < this.getNombreColonne() ) {
                        if (this.getCase(indexLigne, indexColonne).isMine()) {
                            ++mine;
                            
                        }
                    }
                }
            }
        
        this.getCase(ligne, colonne).setNombreMineAutour(mine);
}



    public int getNombreMine(){
        return(this.nombreMines);
    }

    public void setEtatAffichage(int ligne, int colonnes, EtatAffichageModel affichage) {
        this.getCase(ligne, colonnes).setEtatAffichage(affichage);
        if(affichage == EtatAffichageModel.DRAPEAU){
            this.setFlagNumber(this.getFlagNumber() + 1);
        }
        setChanged();
        notifyObservers();
    }

    public EtatAffichageModel getEtatAffichage(int ligne, int colonne) {
        return this.getCase(ligne, colonne).getEtatAffichage();
    }

    public void incrementWellMarkedCase() {
        this.wellMarkedCase++;
    }

    public void decrementWellMarkedCase() {
        this.wellMarkedCase--;
    }

    public int getWellMarkedCase() {
        return this.wellMarkedCase;
    }

    public boolean isWon() {
        return isWon;
    }

    public void setWon(boolean isWon) {
        this.chrono.stop();
        this.isWon = isWon;
        this.setChanged();
        this.notifyObservers();
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public Chronometre getChrono() {
        return chrono;
    }

    public int getTotalTime() {
        return this.chrono.getTotalTime();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
