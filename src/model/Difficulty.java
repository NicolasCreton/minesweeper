package model;

/**
 * Created by Nicolas on 02/12/2014.
 */
public enum Difficulty {
    Beginner("beginner"),
    Intermediate("intermediate"),
    Expert("expert"),
    Custom("custom");

    private String difficulty;

    Difficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getDifficulty() {
        return difficulty;
    }
}
