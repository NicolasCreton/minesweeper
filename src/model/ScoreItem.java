package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Nicolas on 02/12/2014.
 */

/**
 * Objet serializable représenant un score
 */
public class ScoreItem implements Serializable, Comparable<ScoreItem> {
    private Date date;
    private int time;
    private String name;
    private SimpleDateFormat sdt;
    private Difficulty difficulty;

    public ScoreItem(PlateauDemineurModel currentPlateau) {
        this.date = new Date();
        this.time = currentPlateau.getTotalTime();
        this.difficulty = currentPlateau.getDifficulty();
        this.name = currentPlateau.getPlayerName();
    }

    public int getTime() {
        return time;
    }

    public String getDifficulty() {
        return difficulty.getDifficulty();
    }

    @Override
    public String toString() {
        this.sdt = new SimpleDateFormat("EE dd MMMM yyyy hh:mm");
        return name + " | date : " + sdt.format(date) +
                " | time played : " + time +
                " | difficulty : " + difficulty;
    }

    /**
     * Fonction de tri overrdiden, qui est utilisée lorsque il faut trier un tableau de score, classe les temps en ordre croissant
     *
     * @param sc
     * @return
     */
    @Override
    public int compareTo(ScoreItem sc) {
        if (time < sc.time) {
            return -1;
        }

        if (time > sc.time) {
            return 1;
        }
        return 0;
    }
}
