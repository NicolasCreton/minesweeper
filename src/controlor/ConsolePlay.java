package controlor;

import model.EtatAffichageModel;
import model.PlateauDemineurModel;

import java.util.Scanner;

/**
 * Created by nicolas on 29/11/14.
 */
public class ConsolePlay {

    /**
     * Permet d'effectuer les différents mouvements de jeu en console (fait office de parser de commandes)
     * Commence par verifier la première lettre, ensuite la dernière et enfin les coordonnées entre celles ci.
     * Message d'erreur si les commandes sont erronées.
     *
     * @param monPlateau
     * @return
     */
    public static boolean singlePlay(PlateauDemineurModel monPlateau) {
        boolean isFinished = false;

        System.out.print("Entrez vos commandes : ");
        Scanner scanner = new Scanner(System.in);
        String scanValue = scanner.nextLine();
        int[] intArrayCoords;
        intArrayCoords = new int[2];

        String scannedValues[] = scanValue.split(" ");

        if (scannedValues[0].equals("q")) {
            isFinished = true;
        } else if (scannedValues[0].equals("m")) {
            if (scannedValues[3].equals("x")) {
                //Marquer la case i j comme contenant une mine : m i j x
                boolean coords;
                coords = InitializeGameConsole.checkCoords(scannedValues, monPlateau);
                if (coords == true) {
                    //Action a effectuer
                    intArrayCoords[0] = Integer.parseInt(scannedValues[1]);
                    intArrayCoords[1] = Integer.parseInt(scannedValues[2]);
                    monPlateau.setEtatAffichage(intArrayCoords[0], intArrayCoords[1], EtatAffichageModel.DRAPEAU);
                }
            } else if (scannedValues[3].equals("?")) {
                //Marquer la case i j comme étant indécise : m i j ?
                boolean coords;
                coords = InitializeGameConsole.checkCoords(scannedValues, monPlateau);
                if (coords == true) {
                    //Action a effectuer
                    intArrayCoords[0] = Integer.parseInt(scannedValues[1]);
                    intArrayCoords[1] = Integer.parseInt(scannedValues[2]);
                    monPlateau.setEtatAffichage(intArrayCoords[0], intArrayCoords[1], EtatAffichageModel.INDECIS);
                }
            } else if (scannedValues[3].equals("#")) {
                //Supprimer tout marquage sur la case i j : m i j #
                boolean coords;
                coords = InitializeGameConsole.checkCoords(scannedValues, monPlateau);
                //Action a effectuer
                if (coords == true) {
                    //Action a effectuer
                    intArrayCoords[0] = Integer.parseInt(scannedValues[1]);
                    intArrayCoords[1] = Integer.parseInt(scannedValues[2]);
                    monPlateau.setEtatAffichage(intArrayCoords[0], intArrayCoords[1], EtatAffichageModel.RIEN);
                }
            } else {
                System.out.println("Entrez une commande correcte !");
            }
        } else if (scannedValues[0].equals("d")) {
            //Dévoiler la case i j : d i j
            boolean coords;
            coords = InitializeGameConsole.checkCoords(scannedValues, monPlateau);
            //Action a effectuer
            if (coords == true) {
                //Action a effectuer
                intArrayCoords[0] = Integer.parseInt(scannedValues[1]);
                intArrayCoords[1] = Integer.parseInt(scannedValues[2]);
                monPlateau.devoilerCaseAutour(intArrayCoords[0], intArrayCoords[1]);
            }
        } else {
            System.out.println("Entrez une commande correcte !");
        }
        if (monPlateau.isLose(intArrayCoords[0], intArrayCoords[1])) {
            System.out.println("Lost");
            isFinished = true;
        } else if (monPlateau.isNotTheEnd()) {
            System.out.println("Win");
            isFinished = true;
        }

        return isFinished;
    }

    /**
     * Appelle la fonction de jeu tant que la partie n'est pas finie
     * @param monPlateau
     */
    public static void play(PlateauDemineurModel monPlateau) {
        boolean isFinished = false;
        while (isFinished != true) {
            isFinished = singlePlay(monPlateau);

        }
    }
}
