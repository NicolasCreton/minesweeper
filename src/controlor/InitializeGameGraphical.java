package controlor;

import model.Difficulty;
import model.PlateauDemineurModel;
import view.console.PlateauDeJeuConsoleView;
import view.graphique.CustomGamePanel;
import view.graphique.NewGameFrame;
import view.graphique.NewGamePanel;
import view.graphique.PlateauDeJeuVGraphiqueView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Nicolas on 02/12/2014.
 */
public class InitializeGameGraphical implements ActionListener {

    private PlateauDeJeuVGraphiqueView view;
    private NewGamePanel ngp;
    private NewGameFrame ngf;
    private CustomGamePanel cgp;

    /**
     * Lors du clic sur le bouton valider de la fenetre de nouveau jeu, on initialise le jeu en mode graphique
     * Le jeu en mode console se lance simultanément lors du prmeier clic dans une case
     * Va lire la difficulté voulue grace aux radio buttons et instancie le plateau de jeu correspondant.
     * La difficulté est un int de 0 à 4
     *
     * @param ngf
     * @param ngp
     * @param cgp
     */
    public InitializeGameGraphical(NewGameFrame ngf, NewGamePanel ngp, CustomGamePanel cgp) {
        this.ngf = ngf;
        this.ngp = ngp;
        this.cgp = cgp;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (ngp.getGameDifficulty()) {
            case 0:
                JOptionPane.showMessageDialog(null, "You have to select an option first", "Warning", JOptionPane.ERROR_MESSAGE);
                break;
            case 1:
                System.out.println("You chose Beginner");
                PlateauDemineurModel beginnerPlateau = new PlateauDemineurModel(9, 9, 10, Difficulty.Beginner);
                beginnerPlateau.poseDesMines();
                this.view = new PlateauDeJeuVGraphiqueView(beginnerPlateau);
                PlateauDeJeuConsoleView consoleB = new PlateauDeJeuConsoleView(beginnerPlateau);
                ngf.dispose();
                break;
            case 2:
                System.out.println("You chose Intermediate");
                PlateauDemineurModel intermediatePlateau = new PlateauDemineurModel(16, 16, 40, Difficulty.Intermediate);
                intermediatePlateau.poseDesMines();
                PlateauDeJeuConsoleView consoleI = new PlateauDeJeuConsoleView(intermediatePlateau);
                this.view = new PlateauDeJeuVGraphiqueView(intermediatePlateau);
                ngf.setVisible(false);
                break;
            case 3:
                System.out.println("You chose Expert");
                PlateauDemineurModel expertPlateau = new PlateauDemineurModel(16, 30, 99, Difficulty.Expert);
                expertPlateau.poseDesMines();
                PlateauDeJeuConsoleView consoleE = new PlateauDeJeuConsoleView(expertPlateau);
                this.view = new PlateauDeJeuVGraphiqueView(expertPlateau);
                ngf.setVisible(false);
                break;
            case 4:
                System.out.println("You chose Custom ");
                int nbRows = cgp.getRows().getSlider().getValue();
                int nbCols = cgp.getCols().getSlider().getValue();
                int nbMines = cgp.getMines().getSlider().getValue();
                PlateauDemineurModel customPlateau = new PlateauDemineurModel(nbRows, nbCols, nbMines, Difficulty.Custom);
                customPlateau.poseDesMines();
                PlateauDeJeuConsoleView consoleC = new PlateauDeJeuConsoleView(customPlateau);
                this.view = new PlateauDeJeuVGraphiqueView(customPlateau);
                ngf.setVisible(false);
                break;
        }

    }
}
