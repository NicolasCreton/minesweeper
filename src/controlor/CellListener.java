package controlor;

import model.*;
import view.graphique.EndGame;
import view.graphique.NicknamePanel;
import view.graphique.PlateauDeJeuVGraphiqueView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by nicolas.
 *
 * Mouse Listener activé lorsque l'on va cliquer sur un bouton dans le plateau de jeu
 */
public class CellListener implements MouseListener {
    private int indexRow;
    private int indexCol;
    private PlateauDemineurModel currentPlateau;
    private EndGame endOfTheGame;
    private PlateauDeJeuVGraphiqueView plateauGraph;
    private NicknamePanel namePanel;


    /**
     * Constructeur par defaut du Cell Listener
     *
     * @param plateauGraph
     * @param currentPlateau
     * @param indexRow
     * @param indexCol
     */
    public CellListener(PlateauDeJeuVGraphiqueView plateauGraph, PlateauDemineurModel currentPlateau, int indexRow, int indexCol) {
        this.indexRow = indexRow;
        this.currentPlateau = currentPlateau;
        this.indexCol = indexCol;
        this.plateauGraph = plateauGraph;
    }

    /**
     * C'est dans cette fonction que vont s'effectuer les différentes action en fonction du type de click effectué.
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (this.currentPlateau.getCase(indexRow, indexCol).isMine() == true) {
                showAllCase(this.currentPlateau);
                plateauGraph.setEnabled(false);
                this.currentPlateau.setWon(false);
                endOfTheGame = new EndGame("You Lost in " + this.currentPlateau.getChrono().getMinutesString() + " : " + this.currentPlateau.getChrono().getSecondesString(), this.plateauGraph);
            } else {
                this.currentPlateau.devoilerCaseAutour(indexRow, indexCol);
            }
        }
        if (e.getButton() == MouseEvent.BUTTON3) {
            if(this.currentPlateau.getCase(indexRow, indexCol).isCacher() == true){
                if (this.currentPlateau.getEtatAffichage(indexRow, indexCol) == EtatAffichageModel.RIEN) {
                    if (this.currentPlateau.getNombreMine() == this.currentPlateau.getFlagNumber()) {
                        JOptionPane.showMessageDialog(null, "You are out of flag !", "Flag Warning", JOptionPane.WARNING_MESSAGE);
                    } else {
                        this.currentPlateau.setEtatAffichage(indexRow, indexCol, EtatAffichageModel.DRAPEAU);
                        if (this.currentPlateau.getCase(indexRow, indexCol).isMine() == true) {
                            this.currentPlateau.incrementWellMarkedCase();
                        }
                        if (this.currentPlateau.getNombreMine() == this.currentPlateau.getWellMarkedCase()) {
                            plateauGraph.setEnabled(false);
                            this.currentPlateau.setWon(true);
                            this.namePanel = new NicknamePanel();
                            String[] options = namePanel.getOptions();
                            int choice = JOptionPane.showOptionDialog(null, this.namePanel, "Name", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                            if (choice == 0 && !this.namePanel.getTxt().getText().isEmpty()) {
                                this.currentPlateau.setPlayerName(this.namePanel.getTxt().getText());
                            }
                            endOfTheGame = new EndGame("Play again ?", this.plateauGraph);
                            if (this.currentPlateau.getDifficulty() != Difficulty.Custom) {
                                ScoreItem sci = new ScoreItem(this.currentPlateau);
                                Scores scores = new Scores(sci);
                            }
                        }
                    }
                } else if (this.currentPlateau.getEtatAffichage(indexRow, indexCol) == EtatAffichageModel.DRAPEAU) {
                    if (this.currentPlateau.getCase(indexRow, indexCol).isMine() == true) {
                        this.currentPlateau.decrementWellMarkedCase();
                    }
                    this.currentPlateau.decrementFlag();
                    this.currentPlateau.setEtatAffichage(indexRow, indexCol, EtatAffichageModel.INDECIS);
                } else if (this.currentPlateau.getEtatAffichage(indexRow, indexCol) == EtatAffichageModel.INDECIS) {
                    this.currentPlateau.setEtatAffichage(indexRow, indexCol, EtatAffichageModel.RIEN);
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * Dévoile toute les cases du plateu lorsque la partie est perdue
     * @param plateau
     */
    public void showAllCase(PlateauDemineurModel plateau) {
        for (int indexSize = 0; indexSize < plateau.getSize(); indexSize++) {
            plateau.getCasebyIndex(indexSize).setCacher(false);
        }
    }
}
