package controlor;

import model.Difficulty;
import model.PlateauDemineurModel;

import java.util.Scanner;

/**
 * Created by nicolas on 29/11/14.
 */
public class InitializeGameConsole {

    /**
     * Permet d'initialiser le jeu en console, parse les commandes pour créer le plateau de jeu voulu
     *
     * @return
     */
    public static PlateauDemineurModel intitializeGame() {
        boolean checkInt = false;
        PlateauDemineurModel monPlateau = null;
        int nbRow = -1;
        int nbCol = -1;
        int percentMines = -1;
        while (checkInt == false) {
            checkInt = true;
            System.out.print("Entrez le nombre de lignes voulues suivi du nombre de colonnes et du pourcentage de mines voulues : ");
            Scanner scanner = new Scanner(System.in);
            String scanValue = scanner.nextLine();
            String scannedValues[] = scanValue.split(" ");
            try {
                nbRow = Integer.parseInt(scannedValues[0]);
                nbCol = Integer.parseInt(scannedValues[1]);
                percentMines = Integer.parseInt(scannedValues[2]);
            } catch (NumberFormatException nfex) {
                System.out.println("Veuillez respecter les conventions d'entrées : ligne colonne %");
                checkInt = false;
            }
            if (checkInt == true && nbRow > 0 && nbCol > 0 && percentMines <= (0.85*(nbRow*nbCol)) && percentMines > 0) {
                int nbMines = (percentMines*(nbRow*nbCol))/100;
                monPlateau = new PlateauDemineurModel(nbCol, nbRow, nbMines, Difficulty.Custom);
            } else if (checkInt == true && (nbRow <= 0 || nbCol <= 0 || percentMines > 85 || percentMines <= 0)) {
                //Si ce sont bien des int mais qu'ils ne respectent pas les conditions du tableau
                System.out.println("Veuillez ne pas entrer de ligne ou colonnes négatives ou un nombre excessif de mines");
                checkInt = false;
            }
        }
        creeNiveau(monPlateau);
        return monPlateau;
    }

    /**
     * Verification si les coordonnées en entrée sont bien des integers et non autres choses
     * @param scannedValues
     * @param monPlateau
     * @return
     */
    public static boolean checkCoords(String[] scannedValues, PlateauDemineurModel monPlateau) {
        boolean checked = true;
        int[] intArrayCoords;
        intArrayCoords = new int[2];
        try {
            intArrayCoords[0] = Integer.parseInt(scannedValues[1]);
            intArrayCoords[1] = Integer.parseInt(scannedValues[2]);
        } catch (NumberFormatException nfe) {
            System.out.println("Veuillez entrer uniquement des chiffres dans les coordonnées");
            checked = false;
        }
        if (intArrayCoords[0] < 0 || intArrayCoords[1] < 0 || intArrayCoords[0] > monPlateau.getNombreLigne() || intArrayCoords[1] > monPlateau.getNombreColonne()) {
            System.out.println("Veuillez entrez des valeurs ne dépassant pas le tableau.");
            checked = false;
        }

        return checked;
    }


    /**
     * Appelle la fonction de poses des mines, qui va mettre le nombre de mines voulues aléatoirement sur le plateau
     *
     * @param monPlateau
     */
    public static void creeNiveau(PlateauDemineurModel monPlateau) {
        monPlateau.poseDesMines();
    }
}
