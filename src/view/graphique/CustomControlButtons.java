package view.graphique;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Crée un panel de boutons personnalisés (Slider + texte)
 * Va être instancié 3 fois pour créer l'ensemble des 3 sliders dans le custom Game Panel (colonnes, lignes, mines)
 */
public class CustomControlButtons extends JPanel {

    private JSlider slider;
    private JTextField text;
    private int currentNumber;

    public CustomControlButtons(String label, int min, int max, int defaultVal, int majorTick, int minorTick) {
        slider = new JSlider(JSlider.HORIZONTAL, min, max, defaultVal);
        this.add(new JLabel(label));
        slider.setMajorTickSpacing(majorTick);
        slider.setMinorTickSpacing(minorTick);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        this.add(slider);
        text = new JTextField("" + defaultVal);
        text.setPreferredSize(new Dimension(100, 20));
        this.add(text);

        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                setCurrentNumber(slider.getValue());
            }
        });


        text.addKeyListener(new KeyAdapter() {
            //Change la valeur du slider lorsque une touche du clavier est relevée, si un nombre impossible est entré, la valeur du slider est prise.
            public void keyReleased(KeyEvent ke) {
                //Si la valeur d'entré est fausse, on met le texte en rouge et on attrape l'exception
                try {
                    String valueTextField = text.getText();
                    int value = Integer.valueOf(valueTextField);
                    if (value < slider.getMinimum() || value > slider.getMaximum()) {
                        text.setForeground(Color.red);
                        return;
                    } else {
                        text.setForeground(Color.black);
                        setCurrentNumber(value);
                    }
                } catch (NumberFormatException nfe) {
                    System.out.print("Wrong entry");
                    text.setForeground(Color.red);
                }
            }
        });
    }

    private void refreshButtons() {
        slider.setValue(currentNumber);
        text.setText(String.valueOf(this.currentNumber));
    }

    private void setCurrentNumber(int currentNumber) {
        this.currentNumber = currentNumber;
        refreshButtons();
    }

    public void modifySlider(int max) {
        slider.setMaximum(max);
    }

    public JSlider getSlider() {
        return slider;
    }

}
