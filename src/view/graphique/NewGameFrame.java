package view.graphique;

import controlor.InitializeGameGraphical;

import javax.swing.*;
import java.awt.*;
//import java.awt.*;

/**
 * Frame de création de nouveau jeu, contient les instanciations des différents composants graphiques
 */
public class NewGameFrame extends JFrame {
    private NewGamePanel ngp;
    private CustomGamePanel cgp;
    private ValidationButton validate;
    private JButton vbutton;

    public NewGameFrame() {
        this.setLayout(new BorderLayout(0, 0));
        this.setTitle("New Game");
        ngp = new NewGamePanel();
        cgp = new CustomGamePanel();
        validate = new ValidationButton();
        this.add(ngp, BorderLayout.NORTH);
        this.add(cgp, BorderLayout.CENTER);
        this.add(validate, BorderLayout.SOUTH);
        vbutton = this.validate.getValidation();
        vbutton.addActionListener(new InitializeGameGraphical(this, ngp, cgp));
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }



}
