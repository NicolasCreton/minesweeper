package view.graphique;

import javax.swing.*;

/**
 * Panel contenant le bouton de validation sur la frame de nouveau jeu
 */
public class ValidationButton extends JPanel {
    private JButton validation;

    public ValidationButton() {
        validation = new JButton("Start Game");
        this.add(validation);
    }

    public JButton getValidation() {
        return validation;
    }
}
