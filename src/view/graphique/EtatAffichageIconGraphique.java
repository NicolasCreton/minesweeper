/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.graphique;

import javax.swing.*;

/**
 * Enum d'icones affichées sur les case en graphique
 */
public enum EtatAffichageIconGraphique {
    DRAPEAU(new ImageIcon("./img/flags.png")),
    INDECIS(new ImageIcon("./img/inde.png"));
    
    private ImageIcon etat;
    
    private EtatAffichageIconGraphique(ImageIcon etat){
        this.etat = etat;
    }
    
    public ImageIcon getEtatAffichage(){
        return this.etat;
    }
}
