/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.graphique;

import model.PlateauDemineurModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Frame de jeu, contenant la grille, timer, les mines restantes
 */

public class PlateauDeJeuVGraphiqueView extends JFrame {
    private PlateauDemineurModel plateau;
    private SouthPanelView remainingMines;
    private MenuPanelNorth menuBar;
    private JMenu newGameMenu;
    private JMenuItem beginner;
    private JMenuItem intermediate;
    private JMenuItem expert;
    private JMenuItem custom;
    private TimerView timer;
    private JButton restart;

    public PlateauDeJeuVGraphiqueView(PlateauDemineurModel plateau) {
        this.setTitle("Minesweeper");
        this.plateau = plateau;
        this.setLayout(new BorderLayout(5, 5));
        menuBar = new MenuPanelNorth();
        this.add(menuBar, BorderLayout.NORTH);
        GraphicalGridView myGrid = new GraphicalGridView(plateau, this);
        this.add(myGrid, BorderLayout.CENTER);
        timer = new TimerView(this.plateau);
        this.add(timer, BorderLayout.EAST);
        remainingMines = new SouthPanelView(plateau);
        this.add(remainingMines, BorderLayout.SOUTH);
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setLocationRelativeTo(null);

        newGameMenu = this.menuBar.getNewGameMenu();
        newGameMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        beginner = this.menuBar.getItemBeginner();
        beginner.addActionListener(new CloseCurrentFrame());
        intermediate = this.menuBar.getItemIntermediate();
        intermediate.addActionListener(new CloseCurrentFrame());
        expert = this.menuBar.getItemExpert();
        expert.addActionListener(new CloseCurrentFrame());
        custom = this.menuBar.getItemCustom();
        custom.addActionListener(new CloseCurrentFrame());


    }

    private class CloseCurrentFrame implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
        }
    }

}
