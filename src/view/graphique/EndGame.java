package view.graphique;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Frame de fin de jeu
 * instanciée en fin de partie lors d'une perte ou d'une réussite, seul le texte de la frame change (donné en argument)
 * Le focus reste sur elle obligatoirement
 */
public class EndGame extends JFrame {
    private NewGameFrame ngf;
    private PlateauDeJeuVGraphiqueView plateauGraph;

    public EndGame(String type, final PlateauDeJeuVGraphiqueView plateauGraph) {
        this.plateauGraph = plateauGraph;
        this.setTitle("End of the game");
        this.setLayout(new GridLayout(3, 0));
        JLabel lost = new JLabel(type, SwingConstants.CENTER);
        this.add(lost);
        JButton exit = new JButton("Exit");
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        JButton replay = new JButton("Replay");
        replay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ngf = new NewGameFrame();
                dispose();
                plateauGraph.dispose();
            }
        });
        this.add(replay);
        this.add(exit);

        this.setSize(300, 150);
        this.setVisible(true);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

    }
}
