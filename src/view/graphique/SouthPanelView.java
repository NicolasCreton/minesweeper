/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.graphique;

import model.PlateauDemineurModel;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Label sous la grille de jeu, permettant d'afficher le nombre de drapeau restant en jeu
 * Affiche le nombre de drapeau et l'état de fin de partie (Won)
 * Observe le nombre de drapeau posé et le nombre de mines dans le modèle
 */

public class SouthPanelView extends JLabel implements Observer{

    PlateauDemineurModel plateau;
    
    public SouthPanelView(PlateauDemineurModel plateau) {
        this.setHorizontalAlignment(CENTER);
        this.plateau = plateau;
        plateau.addObserver(this);
        int mines = this.getPlateau().getNombreMine() - this.getPlateau().getFlagNumber();
        this.setText("Remaining mines : " + mines);
    }
    
    @Override
    public void update(Observable o, Object arg){
        int mines = this.getPlateau().getNombreMine() - this.getPlateau().getFlagNumber();

        if (this.getPlateau().isWon() == true) {
            this.setText("You Won !");
        } else {
            this.setText("Remaining mines : " + mines);
        }


    }

    public PlateauDemineurModel getPlateau() {
        return plateau;
    }

    
    
    
}
