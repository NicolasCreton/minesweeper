/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.graphique;

import model.CaseModel;
import model.EtatAffichageModel;
import model.PlateauDemineurModel;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Affichage des cases en graphique
 * Affiche les icones des cases
 * et le nombre de mines autour d'une case avec changement de couleur
 */

public class GraphicalCellView extends JButton implements Observer{    
    
    public int positionX;
    public int positoinY;
    PlateauDemineurModel plateau;
    
    public GraphicalCellView(PlateauDemineurModel plateau, CaseModel myCase, int x, int y){
        super();
        this.positionX = x;
        this.positoinY = y;
        this.plateau = plateau;
        myCase.addObserver(this);
    }

    public GraphicalCellView(String string, CaseModel myCase) {
        super(string);
        myCase.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg){
        this.affichageCase();
    }
    
    public void affichageCase(){
        if(this.plateau.getCase(this.positionX, this.positoinY).isCacher()){
            if (this.plateau.getCase(this.positionX, this.positoinY).getEtatAffichage() == EtatAffichageModel.DRAPEAU) {
                this.setIcon(EtatAffichageIconGraphique.DRAPEAU.getEtatAffichage());
            } /*else if (this.plateau.getCase(this.getPositionX(), this.getPositoinY()).isMine() == true) { //Debug only
                this.setText("x");
            }*/ else if (this.plateau.getCase(this.positionX, this.positoinY).getEtatAffichage() == EtatAffichageModel.INDECIS) {
                this.setIcon(EtatAffichageIconGraphique.INDECIS.getEtatAffichage());
            } else if (this.plateau.getCase(this.positionX, this.positoinY).getEtatAffichage() == EtatAffichageModel.RIEN) {
                this.setIcon(null);
                setText(EtatAffichageGraphiqueView.RIEN.getEtatAffichage());
            }
                }else if(this.plateau.getCase(this.positionX, this.positoinY).isMine()){
                    this.setEnabled(false);
                    this.setText("");
                    this.setIcon(new ImageIcon("./img/mine.png"));
                }else if(this.plateau.getCase(this.positionX, this.positoinY).getNombreMineAutour() == 0){
                    this.setEnabled(false);
                    this.setIcon(null);
                    this.setText("");
                }else{
                    this.setEnabled(false);
                    this.setIcon(null);
                    Integer nbMine = this.plateau.getCase(this.positionX, this.positoinY).getNombreMineAutour();
                    if(nbMine == 1){
                        this.setForeground(Color.blue);
                    } else if(nbMine == 2){
                        this.setForeground(Color.green);
                    } else if(nbMine >= 3){
                        this.setForeground(Color.red);
                    }
                    this.setText(nbMine.toString());
                }
    }
    
    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositoinY() {
        return positoinY;
    }

    public void setPositoinY(int positoinY) {
        this.positoinY = positoinY;
    }
    
    
    
    

    
}
