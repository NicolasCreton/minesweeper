package view.graphique;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import view.graphique.SliderChangeListener;

/**
 * Panel contenant les 3 instanciations d'ensemeble de boutons personnalisés (Slider+textField)
 */
public class CustomGamePanel extends JPanel {

    private final int MIN_ROW = 9;
    private final int MAX_ROW = 24;
    private final int MIN_COL = 9;
    private final int MAX_COL = 30;
    private final int MIN_MINES = 10;
    private final int DEFAULT_ROW = 9;
    private final int DEFAULT_COL = 19;
    private final int DEFAULT_MINES = 76;
    private CustomControlButtons rows;
    private int MAX_MINES = (int) (0.85 * (MAX_ROW * MAX_COL));
    private CustomControlButtons cols;
    private CustomControlButtons mines;


    public CustomGamePanel() {

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        //Partie lignes
        rows = new CustomControlButtons("Rows", MIN_ROW, MAX_ROW, DEFAULT_ROW, 3, 1);
        this.add(rows);

        //Parties Colonnes
        cols = new CustomControlButtons("Columns", MIN_COL, MAX_COL, DEFAULT_COL, 3, 1);
        this.add(cols);

        //Parties Mines
        mines = new CustomControlButtons("Mines", MIN_MINES, MAX_MINES, DEFAULT_MINES, 200, 50);
        this.add(mines);

        //Extraction des sliders
        JSlider rowSlider = rows.getSlider();
        JSlider colSlider = cols.getSlider();

        //Ajout des listeners sur les sliders Rows and Cols

        rowSlider.addChangeListener(new ChangeSlider());
        colSlider.addChangeListener(new ChangeSlider());

    }

    public CustomControlButtons getRows() {
        return rows;
    }

    public CustomControlButtons getCols() {
        return cols;
    }

    public CustomControlButtons getMines() {
        return mines;
    }

    private class ChangeSlider implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            int valRow = rows.getSlider().getValue();
            int valCol = cols.getSlider().getValue();
            int max = new Double(0.85 * valCol * valRow).intValue();
            mines.modifySlider(max);
        }
    }


}
