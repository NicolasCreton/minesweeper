package view.graphique;

import model.Difficulty;
import model.ScoreItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;

import static Tools.OsFinder.getOS;

/**
 * Created by Nicolas on 02/12/2014.
 */
public class GraphicalScore extends JFrame {
    private ScoreItem sci;
    private JLabel jLabel[];
    private JLabel title;
    //private String score;
    private File folder;
    private File listOfFiles[];
    private ScoreItem[] allScores;
    private Difficulty difficulty;

    public GraphicalScore(Difficulty difficulty) {
        this.setTitle("Scores");
        try {
            this.difficulty = difficulty;
            if (getOS().equals("win")) {
                this.folder = new File("src\\scores\\" + this.difficulty.getDifficulty());
            } else {
                this.folder = new File("src/scores/" + this.difficulty.getDifficulty());
            }
            this.listOfFiles = this.folder.listFiles();
            if (listOfFiles.length != 0) {
                this.setLayout(new GridLayout(7, 0));
                this.title = new JLabel("Top 5 scores for " + this.difficulty.getDifficulty(), SwingConstants.HORIZONTAL);
                this.add(title);
                this.jLabel = new JLabel[listOfFiles.length];
                this.allScores = new ScoreItem[listOfFiles.length];
                for (int i = 0; i < listOfFiles.length; i++) {
                    File file = listOfFiles[i];
                    if (file.isFile() && file.getName().endsWith(".txt")) {
                        this.allScores[i] = deserializeScore(file);
                    }
                }
                Arrays.sort(this.allScores);
                for (int j = 0; j < allScores.length && j < 5; j++) {
                    this.jLabel[j] = new JLabel(String.valueOf(j + 1) + " : " + allScores[j].toString(), SwingConstants.HORIZONTAL);
                    this.add(jLabel[j]);
                }
                JButton exit = new JButton("OK");
                exit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                    }
                });
                this.add(exit);
                this.setSize(600, 250);
                this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                this.setUndecorated(true);
                this.setVisible(true);
                this.setResizable(false);
                this.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "No game played yet", "No game", JOptionPane.ERROR_MESSAGE);
            }
        } catch (NullPointerException npe) {
            //npe.printStackTrace();
            JOptionPane.showMessageDialog(null, "Check save folder in source code", "Directory", JOptionPane.ERROR_MESSAGE);
        }


    }


    public ScoreItem deserializeScore(File file) {
        // désérialization de l'objet
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            this.sci = (ScoreItem) ois.readObject();
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return sci;
    }
}
