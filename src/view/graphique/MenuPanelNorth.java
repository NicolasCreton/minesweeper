package view.graphique;

import model.Difficulty;
import model.PlateauDemineurModel;
import model.ScoreItem;

import javax.swing.*;
import java.awt.event.*;

/**
 * Menu bar contenant les menus de l'applications
 */

public class MenuPanelNorth extends JMenuBar {

    private JMenu newGameMenu;
    private JMenuItem itemBeginner;
    private JMenuItem itemIntermediate;
    private JMenuItem itemExpert;
    private JMenuItem itemCustom;
    private ScoreItem sci;
    private PlateauDeJeuVGraphiqueView view;
    private NewGameFrame ngf;
    private JMenuItem beginnerItem;



    public MenuPanelNorth() {
        JMenu gameMenu = new JMenu("Game");
        gameMenu.setMnemonic(KeyEvent.VK_G);
        this.add(gameMenu);
        

        newGameMenu = new JMenu("New Game");
        newGameMenu.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ngf = new NewGameFrame();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        gameMenu.add(newGameMenu);



        itemBeginner = new JMenuItem("Beginner");
        itemBeginner.setMnemonic(KeyEvent.VK_B);
        itemBeginner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
        itemBeginner.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlateauDemineurModel beginnerPlateau = new PlateauDemineurModel(9, 9, 10, Difficulty.Beginner);
                beginnerPlateau.poseDesMines();
                view = new PlateauDeJeuVGraphiqueView(beginnerPlateau);

            }
        });
        newGameMenu.add(itemBeginner);

        itemIntermediate = new JMenuItem("Intermediate");
        itemIntermediate.setMnemonic(KeyEvent.VK_I);
        itemIntermediate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
        itemIntermediate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlateauDemineurModel intermediatePlateau = new PlateauDemineurModel(16, 16, 40, Difficulty.Intermediate);
                intermediatePlateau.poseDesMines();
                view = new PlateauDeJeuVGraphiqueView(intermediatePlateau);

            }
        });
        newGameMenu.add(itemIntermediate);

        itemExpert = new JMenuItem("Expert");
        itemExpert.setMnemonic(KeyEvent.VK_E);
        itemExpert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        itemExpert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlateauDemineurModel expertPlateau = new PlateauDemineurModel(16, 30, 99, Difficulty.Expert);
                expertPlateau.poseDesMines();
                view = new PlateauDeJeuVGraphiqueView(expertPlateau);

            }
        });
        newGameMenu.add(itemExpert);

        itemCustom = new JMenuItem("Custom");
        itemCustom.setMnemonic(KeyEvent.VK_C);
        itemCustom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        itemCustom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ngf = new NewGameFrame();
            }
        });
        newGameMenu.add(itemCustom);
        
        JMenuItem quit = new JMenuItem("Quit");
        quit.setMnemonic(KeyEvent.VK_Q);
        quit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        gameMenu.add(quit);

        JMenu scoresMenu = new JMenu("Scores");
        scoresMenu.setMnemonic(KeyEvent.VK_S);
        this.add(scoresMenu);

        JMenuItem scoresItemBeginner = new JMenuItem("View beginner scores");
        //scoresItemBeginner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        scoresItemBeginner.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GraphicalScore graphicalScore = new GraphicalScore(Difficulty.Beginner);
            }
        });
        scoresMenu.add(scoresItemBeginner);
        
        JMenu helpMenu = new JMenu("?");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        this.add(helpMenu);
        
        JMenuItem help = new JMenuItem("Help", KeyEvent.VK_F);
        KeyStroke f1KeyStroke = KeyStroke.getKeyStroke("F1");
        help.setAccelerator(f1KeyStroke);
        help.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "To win, you must put flags on all mines", "Help", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        helpMenu.add(help);

        JMenuItem aboutUs = new JMenuItem("About", KeyEvent.VK_F);
        KeyStroke f2KeyStroke = KeyStroke.getKeyStroke("F2");
        aboutUs.setAccelerator(f2KeyStroke);
        aboutUs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Java Minesweeper created by Nicolas Creton and Edouard Piette, CIR3, ISEN LILLE, 2014-2015", "About", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        helpMenu.add(aboutUs);

        JMenuItem scoresItemInter = new JMenuItem("View Intermediate scores");
        //scoresItemInter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        scoresItemInter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GraphicalScore graphicalScore = new GraphicalScore(Difficulty.Intermediate);
            }
        });
        scoresMenu.add(scoresItemInter);

        JMenuItem scoresItemExpert = new JMenuItem("View Expert scores");
        //scoresItemExpert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        scoresItemExpert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GraphicalScore graphicalScore = new GraphicalScore(Difficulty.Expert);
            }
        });
        scoresMenu.add(scoresItemExpert);

    }


    public JMenuItem getItemBeginner() {
        return itemBeginner;
    }

    public JMenuItem getItemIntermediate() {
        return itemIntermediate;
    }

    public JMenuItem getItemExpert() {
        return itemExpert;
    }

    public JMenuItem getItemCustom() {
        return itemCustom;
    }

    public JMenu getNewGameMenu() {
        return newGameMenu;
    }
}
