package view.graphique;

import model.Chronometre;
import model.PlateauDemineurModel;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Panel affichant le chrono
 * Observe la classe chrono et se met à jour à chaque incrémentation de celle-ci
 * Affichage sous forme minutes:secondes
 */
public class TimerView extends JPanel implements Observer {
    private PlateauDemineurModel plateau;
    private Chronometre chrono;
    private JLabel timer;
    private int secondes = 0;
    private int minutes = 0;

    public TimerView(PlateauDemineurModel plateau) {
        this.plateau = plateau;
        JLabel label = new JLabel("Timer :");
        this.add(label);
        timer = new JLabel(String.valueOf("0" + this.minutes + " : " + "0" + this.secondes));
        this.chrono = this.plateau.getChrono();
        chrono.addObserver(this);
        this.add(timer);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Chronometre) {
            if (((Chronometre) o).getMinutes() < 10) {
                timer.setText("0" + ((Chronometre) o).getMinutesString() + " : " + ((Chronometre) o).getSecondesString());
                if (((Chronometre) o).getSecondes() < 10) {
                    timer.setText("0" + ((Chronometre) o).getMinutesString() + " : " + "0" + ((Chronometre) o).getSecondesString());
                }
            } else {
                timer.setText(((Chronometre) o).getMinutesString() + ":" + ((Chronometre) o).getSecondesString());
            }

        }
    }
}

