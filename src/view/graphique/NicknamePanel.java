package view.graphique;

import javax.swing.*;

/**
 * Panel utilisé dans la boite de dialogue demandant le pseudo du joueur à la fin de sa partie
 * Créé dans le but de pouvoir choisir les boutons voulus à la place du classique OK, CANCEL
 */
public class NicknamePanel extends JPanel {
    private String[] options = {"OK", "Leave blank"};
    private JTextField txt;

    public NicknamePanel() {
        JLabel lbl = new JLabel("Enter Your nickname: ");
        this.txt = new JTextField(10);
        this.add(lbl);
        this.add(txt);
    }

    public String[] getOptions() {
        return options;
    }

    public JTextField getTxt() {
        return txt;
    }
}
