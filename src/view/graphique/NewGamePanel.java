package view.graphique;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel de choix de nouveau jeu, contenant les radio de boutons pour choisir la difficuté
 */
public class NewGamePanel extends JPanel {
    private final String beginner = "Beginner : 10 mines in a 9x9 field";
    private final String intermediate = "Intermediate : 40 mines in a 16x16 field";
    private final String expert = "Expert : 99 mines in a 16x30 field";
    private int gameDifficulty = 0;

    public NewGamePanel() {

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(new JLabel("Select a level :"));
        this.add(new JLabel(" "));
        JRadioButton beginnerRadio = new JRadioButton(beginner);
        beginnerRadio.addActionListener(new RadioListener(1));
        JRadioButton intermediateRadio = new JRadioButton(intermediate);
        intermediateRadio.addActionListener(new RadioListener(2));
        JRadioButton expertRadio = new JRadioButton(expert);
        expertRadio.addActionListener(new RadioListener(3));
        JRadioButton customRadio = new JRadioButton("Custom : ");
        customRadio.addActionListener(new RadioListener(4));
        ButtonGroup groupRadio = new ButtonGroup();
        groupRadio.add(beginnerRadio);
        groupRadio.add(intermediateRadio);
        groupRadio.add(expertRadio);
        groupRadio.add(customRadio);
        this.add(beginnerRadio);
        this.add(intermediateRadio);
        this.add(expertRadio);
        this.add(customRadio);


    }

    public int getGameDifficulty() {
        return gameDifficulty;
    }

    public void setGameDifficulty(int diff) {
        this.gameDifficulty = diff;
    }

    //For Debug Only
    public void displayGameDifficulty() {
        System.out.println(this.gameDifficulty);
    }

    private class RadioListener implements ActionListener {
        private int difficulty;

        public RadioListener(int difficulty) {
            this.difficulty = difficulty;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setGameDifficulty(this.difficulty);
            //displayGameDifficulty(); //For Debug Only
        }
    }
}
