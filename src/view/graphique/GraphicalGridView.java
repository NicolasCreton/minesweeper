/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.graphique;

import controlor.CellListener;
import model.PlateauDemineurModel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel contenant la grille de boutons en graphique
 */

public class GraphicalGridView extends JPanel {
    public List<JButton> cellButtons;
    public int nombreColonne;
    public int nombreLigne;
    public PlateauDemineurModel plateau;

    public GraphicalGridView(PlateauDemineurModel plateau, PlateauDeJeuVGraphiqueView platGraph) {
        this.plateau = plateau;
        this.nombreLigne = this.plateau.getNombreLigne();
        this.nombreColonne = this.plateau.getNombreColonne();
        this.setLayout(new GridLayout(nombreLigne, nombreColonne, 2, 2));
        this.cellButtons = new ArrayList<JButton>();

        for (int i = 0; i < this.plateau.getNombreLigne(); ++i) {
            for (int j = 0; j < this.plateau.getNombreColonne(); ++j) {

                GraphicalCellView cell = new GraphicalCellView(plateau, plateau.getCase(i, j), i, j);
                this.add(cell);
                this.cellButtons.add(cell);
                cell.addMouseListener(new CellListener(platGraph ,this.plateau, cell.getPositionX(), cell.getPositoinY()));
                cell.affichageCase();
                  
            }
        }
    }

    public PlateauDemineurModel getPlateau() {
        return this.plateau;
    }

    public int getNombreColonne() {
        return nombreColonne;
    }

    public void setNombreColonne(int nombreColonne) {
        this.nombreColonne = nombreColonne;
    }

    public int getNombreLigne() {
        return nombreLigne;
    }

    public void setNombreLigne(int nombreLigne) {
        this.nombreLigne = nombreLigne;
    }

    public JButton getCellButtons(int indexLigne, int indexColonne) {
        return this.cellButtons.get(indexLigne * this.getNombreColonne() + indexColonne);
    }

    public void setNameCellButton(int indexLigne, int indexColonne, String name) {
        this.getCellButtons(indexLigne, indexColonne).setText(name);
    }

    public void csetIconCellButton(int indexLigne, int indexColonne, Icon icon) {
        this.getCellButtons(indexLigne, indexColonne).setIcon(icon);
    }
  
    
}
