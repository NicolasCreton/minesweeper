/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.console;

import model.EtatAffichageModel;
import model.PlateauDemineurModel;

import java.util.Observable;
import java.util.Observer;

/**
 * Génération de la vue console du plateau de jeu.
 * Observe les changements du modèle et se met à jour si besoin
 */
public class PlateauDeJeuConsoleView implements Observer{
    private PlateauDemineurModel plateau;

    public PlateauDeJeuConsoleView(PlateauDemineurModel plateau) {
        this.plateau = plateau;
        plateau.addObserver(this);
    }
    
    @Override
    public void update(Observable o, Object arg){
        this.affichagePlateauDeJeuConsole();
    }
    
    public void affichagePlateauDeJeuConsole(){
        for(int indexLigne = 0; indexLigne < this.plateau.getNombreLigne(); ++indexLigne){
            for(int indexColonne = 0; indexColonne < this.plateau.getNombreColonne(); ++indexColonne){
                if(this.plateau.getCase(indexLigne, indexColonne).isCacher()){
                    if(this.plateau.getCase(indexLigne, indexColonne).getEtatAffichage() == EtatAffichageModel.DRAPEAU){
                        System.out.print(EtatAffichageGraphiqueView.DRAPEAU.getEtatAffichage());
                    }
                    /*else if(this.plateau.getCase(indexLigne, indexColonne).isMine()){
                        System.out.print("+");
                    }*/ //Affiche les mines pour le debuggage
                    else if(this.plateau.getCase(indexLigne, indexColonne).getEtatAffichage() == EtatAffichageModel.INDECIS){
                        System.out.print(EtatAffichageGraphiqueView.INDECIS.getEtatAffichage());
                    }else if(this.plateau.getCase(indexLigne, indexColonne).getEtatAffichage() == EtatAffichageModel.RIEN){
                        System.out.print(EtatAffichageGraphiqueView.RIEN.getEtatAffichage());
                    }
                }else if(this.plateau.getCase(indexLigne, indexColonne).isMine()){
                    System.out.print("x");
                }else if(this.plateau.getCase(indexLigne, indexColonne).getNombreMineAutour() == 0){
                    System.out.print(".");
                }else{
                    System.out.print(this.plateau.getCase(indexLigne, indexColonne).getNombreMineAutour());
                }
            }
            System.out.println(" ");
        }
        System.out.println(" ");
    }
}
