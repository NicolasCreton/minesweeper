/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.console;

/**
 *Etat d'affichage d'une case, remplacée après par des icones
 *
 */
public enum EtatAffichageGraphiqueView {
    RIEN("#"),
    DRAPEAU("!"),
    INDECIS("?");
    
    private String etat;
    
    private EtatAffichageGraphiqueView(String etat){
        this.etat = etat;
    }
    
    public String getEtatAffichage(){
        return this.etat;
    }
}
